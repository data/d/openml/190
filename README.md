# OpenML dataset: mbagrade

https://www.openml.org/d/190

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown -   
**Please cite**:   

Dataset from Smoothing Methods in Statistics 
 (ftp stat.cmu.edu/datasets)

 Simonoff, J.S. (1996). Smoothing Methods in Statistics. New York: Springer-Verlag.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/190) of an [OpenML dataset](https://www.openml.org/d/190). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/190/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/190/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/190/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

